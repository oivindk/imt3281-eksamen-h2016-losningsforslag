package no.ntnu.imt3281.starWars;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

public class PeopleTest {
	String json = "{\n"
			+"  \"name\": \"Leia Organa\",\n"
			+"	\"height\": \"150\",\n"
			+"	\"mass\": \"49\",\n"
			+"	\"hair_color\": \"brown\",\n"
			+"	\"skin_color\": \"light\",\n"
			+"	\"eye_color\": \"brown\",\n"
			+"	\"birth_year\": \"19BBY\",\n"
			+"	\"gender\": \"female\",\n"
			+"	\"homeworld\": \"http://swapi.co/api/planets/2/\",\n"
			+"	\"films\": [\n"
			+"		\"http://swapi.co/api/films/6/\",\n"
			+"		\"http://swapi.co/api/films/3/\",\n"
			+"		\"http://swapi.co/api/films/2/\",\n"
			+"		\"http://swapi.co/api/films/1/\",\n"
			+"		\"http://swapi.co/api/films/7/\"\n"
			+"	],\n"
			+"	\"species\": [\n"
			+"		\"http://swapi.co/api/species/1/\"\n"
			+"	],\n"
			+"	\"vehicles\": [\n"
			+"		\"http://swapi.co/api/vehicles/30/\"\n"
			+"	],\n"
			+"	\"starships\": [],\n"
			+"	\"created\": \"2014-12-10T15:20:09.791000Z\",\n"
			+"	\"edited\": \"2014-12-20T21:17:50.315000Z\",\n"
			+"	\"url\": \"http://swapi.co/api/people/5/\"\n"
			+"}";
 	
	@Test
	public void testConstructors() {
		// Create object with id and json string
		// Should create/update record in database.
		People people = new People("people/5/", json);	// ID and json string
		assertEquals(people.getId(), "people/5/");
		assertEquals(people.getName(), "Leia Organa");
		assertEquals(people.getName(), people.toString());
		assertEquals(people.getHeight(), 150, 0);
		assertEquals(people.getMass(), 49, 0);
		assertEquals(people.getHairColor(), "brown");
		assertEquals(people.getSkinColor(), "light");
		assertEquals(people.getEyeColor(), "brown");
		assertEquals(people.getBirthYear(), "19BBY");
		assertEquals(people.getGender(), "female");
		assertTrue(people.getHomeworld() instanceof Planet);
		assertTrue(people.getFilms() instanceof ArrayList<?>);
		assertTrue(people.getFilms().get(0) instanceof Film);
		assertEquals(people.getFilms().size(), 5, 0);
		assertTrue(people.getSpecies() instanceof ArrayList<?>);
		assertTrue(people.getSpecies().get(0) instanceof Species);
		assertEquals(people.getSpecies().size(), 1, 0);
		assertTrue(people.getVehicles() instanceof ArrayList<?>);
		assertTrue(people.getVehicles().get(0) instanceof Vehicle);
		assertEquals(people.getVehicles().size(), 1, 0);
		assertTrue(people.getStarships() instanceof ArrayList<?>);
		assertEquals(people.getStarships().size(), 0, 0);
		
		// Create object with id only.
		// Should read record from database if exists or from 
		// network if not exists. Since we just added it above it should be in the database.
		People leia = new People("people/5/");	// ID and json string
		assertEquals(people, leia);				// Must override equals in People
	}
}
