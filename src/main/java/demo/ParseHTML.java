
package demo;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class ParseHTML {
	public static void main(String[] args) {
		try {
			Document doc = Jsoup.connect("http://starwars.wikia.com/wiki/Naboo").get();
			Element e = doc.select("#WikiaMainContent aside figure a img").first();
			System.out.println(e.attr("src"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
