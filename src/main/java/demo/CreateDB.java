package demo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateDB {
	private static final String URL = "jdbc:derby:StarWarsDB";
	private static Connection con;
	
	public static void main(String[] args) {
		try {
            con = DriverManager.getConnection(URL);
		} catch (SQLException sqle) {		// No database exists
			try {							// Try creating database
				con = DriverManager.getConnection(URL+";create=true");
				setupDB();
			} catch (SQLException sqle1) {	// Unable to create database, exit server
				System.err.println("Unable to create database"+sqle1.getMessage());
				System.exit(-1);
			}
		}
	}

	private static void setupDB() throws SQLException {
		Statement stmt = con.createStatement();
		stmt.execute("CREATE TABLE people (id char(64) NOT NULL, "
                + "name varchar(128) NOT NULL, "
                + "height int NOT NULL, "
                + "mass float NOT NULL, "
                + "hairColor varchar(32) NOT NULL, "
                + "skinColor varchar(32) NOT NULL, "
                + "eyeColor varchar(32) NOT NULL, "
                + "birthYear varchar(32) NOT NULL, "
                + "gender varchar(16) NOT NULL, "
                + "homeworld varchar(64) NOT NULL, "
                + "films long varchar NOT NULL, "		// Stored as string with comma separated film id's
                + "species long varchar NOT NULL, "		// Stored as string with comma separated species id's
                + "vehicles long varchar NOT NULL, "		// Stored as string with comma separated vehicle id's
                + "starships long varchar NOT NULL, "	// Stored as string with comma separated starship id's
                + "PRIMARY KEY  (id))");
		stmt.execute("CREATE TABLE film (id char(64) NOT NULL, "
				+ "title varchar(128) NOT NULL, "
				+ "episodeId int not null, "
				+ "openingCrawl long varchar NOT NULL, "
				+ "director varchar(128) NOT NULL,"
				+ "producer varchar(128) NOT NULL,"
				+ "releaseDate varchar(16) NOT NULL,"
				+ "characters long varchar NOT NULL,"
				+ "planets long varchar NOT NULL,"
				+ "starships long varchar NOT NULL,"
				+ "vehicles long varchar NOT NULL,"
				+ "species long varchar NOT NULL,"
				+ "PRIMARY KEY (id))");
		stmt.execute("CREATE TABLE planet (id char(64) NOT NULL,"
				+ "name varchar(128) NOT NULL, "
				+ "rotationPeriod int NOT NULL, "
				+ "orbitalPeriod int NOT NULL, "
				+ "diameter int NOT NULL, "
				+ "climate varchar(64) NOT NULL, "
				+ "gravity varchar(64) NOT NULL, "
				+ "terrain varchar(64) NOT NULL, "
				+ "surfaceWater int NOT NULL, "
				+ "population bigint NOT NULL, "						// NOTE: int is not enough
				+ "residents long varchar NOT NULL, "
				+ "films long varchar NOT NULL, "
				+ "PRIMARY KEY(id))");
		stmt.execute("CREATE TABLE starship (id char(64) NOT NULL, "
				+ "name varchar(64) NOT NULL, "
				+ "model varchar(128) NOT NULL, "
				+ "manufacturer varchar(64) NOT NULL,"
				+ "cost double NOT NULL, "
				+ "length float NOT NULL, "
				+ "maxAtmospheringSpeed varchar(32) NOT NULL, "
				+ "crew int NOT NULL, "
				+ "passengers int NOT NULL, "
				+ "cargoCapacity bigint NOT NULL, "						// NOTE: int is not enough
				+ "consumables varchar(64) NOT NULL, "
				+ "hyperdriveRating float NOT NULL, "
				+ "MGLT int NOT NULL,"
				+ "starshipClass varchar(64) NOT NULL,"
				+ "pilots long varchar NOT NULL, "
				+ "films long varchar NOT NULL, "
				+ "PRIMARY KEY(id))");
		stmt.execute("CREATE TABLE species (id char(64) NOT NULL, "
				+ "name varchar(64) NOT NULL, "
				+ "classification varchar(64) NOT NULL, "
				+ "designation varchar(32) NOT NULL, "
				+ "averageHeight int NOT NULL, "
				+ "skinColors varchar(64) NOT NULL, "
				+ "hairColors varchar(64) NOT NULL, "
				+ "eyeColors varchar(64) NOT NULL, "
				+ "averageLifespan varchar(32) NOT NULL, "
				+ "homeworld varchar(64),"
				+ "language varchar(32) NOT NULL,"
				+ "people long varchar NOT NULL,"
				+ "films long varchar NOT NULL,"
				+ "PRIMARY KEY(id))");
		stmt.execute("CREATE TABLE vehicle (id char(64) NOT NULL,"
				+ "name varchar(64) NOT NULL,"
				+ "model varchar(64) NOT NULL,"
				+ "manufacturer varchar(64) NOT NULL,"
				+ "cost double NOT NULL,"
				+ "length double NOT NULL, "
				+ "maxAtmospheringSpeed varchar(32) NOT NULL, "
				+ "crew int NOT NULL,"
				+ "passengers int NOT NULL,"
				+ "cargoCapacity int NOT NULL,"
				+ "consumables varchar(32) NOT NULL,"
				+ "vehicleClass varchar(32) NOT NULL,"
				+ "pilots long varchar NOT NULL,"
				+ "films long varchar NOT NULL,"
				+ "PRIMARY KEY(id))");
	}
	
	
}
