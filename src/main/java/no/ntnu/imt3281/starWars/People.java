package no.ntnu.imt3281.starWars;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Used to store information about a person.
 * Object can be created from json string, then the person will be added/updated in the database
 * or just from an id. If only id is given and the person does not exists it will be read from 
 * the net(http://swapi.co/api/) and added to the dabase using the constructor that takes 
 * the id and json data as paramters.
 * 
 * @author oivindk
 *
 */
public class People extends Item {

	/**
	 * Takes an id as parameter, try to read that person from the database. If no such person exists
	 * in the database read it from "http://swapi.co/api/" and run the two parameter constructor to 
	 * insert it into the database.
	 * 
	 * @param id the id of the person/character
	 */
	public People(String id) {
		super(id);
	}

	/**
	 * Takes an id and a json string as parameters. Populates the variables of the object with
	 * data from the json string and stores it/updates it in the database.
	 * 
	 * @param id the id of the person, ie. people/1/
	 * @param json json encoded data for this person/charachter
	 */
	public People(String id, String json) {
		super(id, json);
	}

	/**
	 * Read a character from the database.
	 */
	public void readFromDB() {
		String sql = "SELECT name, height, mass, hairColor, skinColor, eyeColor, birthYear, "
				+ "gender, homeworld, films, species, vehicles, starships FROM people WHERE id=?";
		ResultSet res = DB.select(sql, id);
		try {
			if (res!=null&&res.next()) {
				data.put("name", res.getString("name"));
				data.put("height", res.getInt("height"));
				data.put("mass", res.getInt("mass"));
				data.put("hair_color",  res.getString("hairColor"));
				data.put("skin_color", res.getString("skinColor"));
				data.put("eye_color", res.getString("eyeColor"));
				data.put("birth_year", res.getString("birthYear"));
				data.put("gender", res.getString("gender"));
				data.put("homeworld", res.getString("homeworld"));
				data.put("films", stringArrayRepresentationToArrayList(res.getString("films")));
				data.put("species", stringArrayRepresentationToArrayList(res.getString("species")));
				data.put("vehicles", stringArrayRepresentationToArrayList(res.getString("vehicles")));
				data.put("starships", stringArrayRepresentationToArrayList(res.getString("starships")));
			} else {
				readFromSwapi();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Update or write a character to the database.
	 */
	public void writeToDB() {
		String updateSQL = "UPDATE people SET "
				+ "name=?, height=?, mass=?, hairColor=?, "
				+ "skinColor=?, eyeColor=?, birthYear=?, "
				+ "gender=?, homeworld=?, films=?, species=?, "
				+ "vehicles=?, starships=? WHERE id=?";
		String insertSQL = "INSERT INTO people (name, height, mass, "
				+ "hairColor, skinColor, eyeColor, birthYear, gender, "
				+ "homeworld, films, species, vehicles, starships, id) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		@SuppressWarnings("unchecked")
		Object values[] = {data.get("name"), getHeight(), getMass(), 
				data.get("hair_color"), data.get("skin_color"), data.get("eye_color"), data.get("birth_year"), data.get("gender"),
				data.get("homeworld"), ((ArrayList<String>)data.get("films")).toString(), ((ArrayList<String>)data.get("species")).toString(),
				((ArrayList<String>)data.get("vehicles")).toString(), ((ArrayList<String>)data.get("starships")).toString(), id };
		
		DB.updateInsert(updateSQL, insertSQL, values);
	}	
}
