package no.ntnu.imt3281.starWars;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Used to hold information films, people, planets, species, starships and vehicles from star wars.
 * This is the base class for those respective classes.
 * 
 * @author oivindk
 *
 */
public abstract class Item {
	HashMap<String, Object> data = new HashMap<String, Object>();
	String id;
	protected Planet planet = null;
	protected ArrayList<Film> films = null;
	protected ArrayList<Species> species = null;
	protected ArrayList<Vehicle> vehicles = null;
	protected ArrayList<Starship> starships = null;
	protected ArrayList<People> characters = null;
	protected ArrayList<Planet> planets = null;
	protected ArrayList<People> pilots = null;

	/**
	 * Read item from database using the child class's readFromDB method. This will initialize 
	 * the data members based on what child class is created (Planet, People, Film etc.)
	 * 
	 * @param id the id of the resource to initialize.
	 */
	public Item(String id) {
		if (id.startsWith("http://swapi.co/api/"))
			id = id.substring(20);
		this.id = id;
		readFromDB();
	}

	/**
	 * To be overridden in Planet, Film, People etc. Will initialize the item with data for 
	 * the correct class.
	 */
	public abstract void readFromDB ();
	
	/**
	 * Create an item based on given json data. Data will be updated in the database (or 
	 * written to the database if no such record exists) using the child class's writeToDB
	 * method. This ensures that the correct data is written to the correct database table.
	 * 
	 * @param id the id of the resource to initialize.
	 * @param json the json encoded data for this item.
	 */
	@SuppressWarnings("unchecked")
	public Item (String id, String json) {
		this.id = id;
		parseJSON(json);
	}
	
	private void parseJSON(String json) {
		JSONParser parser = new JSONParser();
		try {
			JSONObject obj = (JSONObject) parser.parse(json);
			Iterator<String> iterator = obj.keySet().iterator();
			while (iterator.hasNext()) {
				String key = iterator.next();
				if (obj.get(key) instanceof JSONArray) {
					ArrayList<String> arrayList = new ArrayList<>();  
					JSONArray array = (JSONArray) obj.get(key);
			    	Iterator<String> arrayIterator = array.iterator();
			    	while (arrayIterator.hasNext()) {			// Iterate over the array
			    		arrayList.add(arrayIterator.next());	// Add string members to array
			    	}
			    	data.put(key, arrayList);					// Add list of values for given key (planets, characters, starships, pilots etc.)
				} else {
					data.put(key, obj.get(key));				// Add key/value pair (name, title, director, model, manufacturer etc.)
				}
			}
			if (obj.get("length")!=null) {
				String tmp = obj.get("length").toString();
				tmp = tmp.replaceAll(",", "");
				data.put("length", tmp);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		writeToDB();
	}
	
	protected void readFromSwapi() {
		URL url;
		if (id==null||id.equals(""))
			return;
		try {
			url = new URL ("http://swapi.co/api/"+id);
			URLConnection connection = url.openConnection();
			// Must set user-agent, the java default user agent is denied
			connection.setRequestProperty("User-Agent", "curl/7.8 (i386-redhat-linux-gnu) libcurl 7.8 (OpenSSL 0.9.6b) (ipv6 enabled)");
			// Must set accept to application/json, if not html is returned
			connection.setRequestProperty("Accept", "application/json");
			connection.connect();
			// Read from this input stream
			InputStream is = connection.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			StringBuffer sb = new StringBuffer();
			String tmp = null;
			while ((tmp=br.readLine())!=null) {
				sb.append(tmp);
				sb.append("\n");
			}
			parseJSON(sb.toString());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Used to convert a string representation of an array ([item1, item2, item3]) to
	 * an ArrayList of strings.
	 * 
	 * @param arrayString the string representation of the array
	 * @return the ArrayList of strings with content from the arrayString param.
	 */
	protected ArrayList<String> stringArrayRepresentationToArrayList(String arrayString) {
		arrayString = arrayString.substring(1, arrayString.length()-1);
		ArrayList<String> res = new ArrayList<>();
		for (String tmp : arrayString.split(","))
			res.add(tmp.trim());
		return res;
	}
	
	public abstract void writeToDB ();
	
	/**
	 * Returns id of this item
	 * 
	 * @return id of this item, ie. people/1/, planets/2/, species/2/ 
	 */
	public String getId() {
		return id;
	}

	/**
	 * Return the name of the item. 
	 * If no name exists but title exists then return the title.
	 * 
	 * @return the name of the item.
	 */
	public String getName() {
		if (data.get("name")==null&&data.get("title")!=null)
			return data.get("title").toString();
		return data.get("name").toString();
	}	

	/**
	 * Return the title of the item (this only exists for films). 
	 * 
	 * @return the title of the item.
	 */
	public String getTitle() {
		return data.get("title").toString();
	}	

	/**
	 * Return the opening crawl of the item (this only exists for films). 
	 * 
	 * @return the opening crawl of the film.
	 */
	public String getOpeningCrawl() {
		return data.get("opening_crawl").toString();
	}	

	/**
	 * Return the director of the item (this only exists for films). 
	 * 
	 * @return the director of the film.
	 */
	public String getDirector() {
		return data.get("director").toString();
	}
	
	/**
	 * Return the producer of the item (this only exists for films). 
	 * 
	 * @return the producer of the film.
	 */
	public String getProducer() {
		return data.get("producer").toString();
	}

	/**
	 * Return the release date of the item (this only exists for films). 
	 * 
	 * @return the release date of the film.
	 */
	public String getReleaseDate() {
		return data.get("release_date").toString();
	}

	/**
	 * Returns the name of the item (easy to use in drop down lists.)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return getName();
	}

	/**
	 * Returns the episode id of the item (film).
	 * 
	 * @return the episode id of the item.
	 */
	public int getEpisodeId() {
		return Integer.parseInt(data.get("episode_id").toString());
	}

	/**
	 * Returns the height of the item.
	 * 
	 * @return the height of the item.
	 */
	public int getHeight() {
		if (data.get("height").toString().equals("unknown"))
			return -1;
		return Integer.parseInt(data.get("height").toString());
	}

	/**
	 * Returns the mass of the item (ie. the weight)
	 * 
	 * @return the mass of the item. Return -1 for unknown
	 */
	public float getMass() {
		if (data.get("mass").toString().equals("unknown"))
			return -1;
		String mass = data.get("mass").toString().replace(",", "");
		return Float.parseFloat(mass);
	}

	/**
	 * Returns the hair color of the item
	 * 
	 * @return the hair color of the item.
	 */
	public String getHairColor() {
		return data.get("hair_color").toString();
	}

	/**
	 * Returns the skin color of the item
	 * 
	 * @return the skin color of the item.
	 */
	public String getSkinColor() {
		return data.get("skin_color").toString();
	}

	/**
	 * Returns the eye color of the item
	 * 
	 * @return the eye color of the item.
	 */
	public String getEyeColor() {
		return data.get("eye_color").toString();
	}

	/**
	 * Returns the birth year of the item
	 * 
	 * @return the birth year of the item.
	 */
	public String getBirthYear() {
		return data.get("birth_year").toString();
	}

	/**
	 * Returns the gender of the item
	 * 
	 * @return the gender of the item.
	 */
	public String getGender() {
		return data.get("gender").toString();
	}

	/**
	 * Returns the planet that this item relates to
	 * 
	 * @return the planet the item relates to
	 */
	public Planet getPlanet() {
		if (planet==null)
			planet = new Planet (data.get("planet").toString());
		return planet;
	}

	/**
	 * Returns the homeworld for this item 
	 * 
	 * @return the homeworld for this item
	 */
	public Planet getHomeworld() {
		if (planet==null)
			planet = new Planet (data.get("homeworld").toString());
		return planet;
	}
	
	/**
	 * Get the model (of starship/vehicle)
	 * 
	 * @return the model
	 */
	public String getModel() {
		return data.get("model").toString();
	}
	
	/**
	 * Get the manufacturer (of starship/vehicle)
	 * 
	 * @return the manufacturer
	 */
	public String getManufacturer() {
		return data.get("manufacturer").toString();
	}
	
	/**
	 * Get the cost (of starship/vehicle)
	 * 
	 * @return the cost
	 */
	public double getCost() {
		if (data.get("cost_in_credits")==null)
			return -1;
		if (data.get("cost_in_credits").toString().equals("unknown"))
			return -1;
		return Double.parseDouble(data.get("cost_in_credits").toString());
	}
	
	/**
	 * Get the cost (of starship/vehicle)
	 * 
	 * @return the cost
	 */
	public float getLength() {
		if (data.get("length").toString().equals("unknown"))
			return -1;
		return Float.parseFloat(data.get("length").toString());
	}
	
	/**
	 * Get the size of the crew (of starship/vehicle)
	 * 
	 * @return the size of the crew
	 */
	public int getCrew() {
		if (data.get("crew").toString().equals("unknown"))
			return -1;
		return Integer.parseInt(data.get("crew").toString());
	}
	
	/**
	 * Get the maximum numbers of passengers (of starship/vehicle)
	 * 
	 * @return the maximum number of passengers
	 */
	public int getPassengers() {
		if (data.get("passengers").toString().equals("unknown"))
			return -1;
		return Integer.parseInt(data.get("passengers").toString());
	}
	
	/**
	 * Get the cargo capacity (of starship/vehicle)
	 * 
	 * @return the cargoo capacity
	 */
	public long getCargoCapacity() {
		if (data.get("cargo_capacity").toString().equals("unknown"))
			return -1;
		if (data.get("cargo_capacity").toString().equals("none"))
			return 0;
		return Long.parseLong(data.get("cargo_capacity").toString());
	}
	
	/**
	 * Get the max atmosphering speed (of starship/vehicle)
	 * 
	 * @return the max atmosphering speed
	 */
	public String getMaxAtmospheringSpeed() {
		return data.get("max_atmosphering_speed").toString();
	}
	
	/**
	 * Get the consumables (of starship/vehicle)
	 * 
	 * @return the consumables for starship/vehicle
	 */
	public String getConsumables() {
		return data.get("consumables").toString();
	}
	
	/**
	 * Get the vehicle class (of starship/vehicle)
	 * 
	 * @return the vehicle class
	 */
	public String getVehicleClass() {
		return data.get("vehicle_class").toString();
	}
	
	/**
	 * Returns the characters relevant to this item 
	 * 
	 * @return the characters relevant to this item
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<People> getCharacters() {
		if (characters==null) {
			characters = new ArrayList<>();
			((ArrayList<String>)data.get("characters")).forEach(e->characters .add(new People(e)));
		}
		return characters;
	}
	
	/**
	 * Returns the films the item appear in
	 * 
	 * @return the films the item appear in
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Film> getFilms() {
		if (films==null) {
			films = new ArrayList<>();
			((ArrayList<String>)data.get("films")).forEach(e->films.add(new Film(e)));
		}
		return films;
	}

	/**
	 * Returns the species relevant to this item
	 * 
	 * @return the species relevant to this item
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Species> getSpecies() {
		if (species==null) {
			species = new ArrayList<>();
			((ArrayList<String>)data.get("species")).forEach(e->species.add(new Species(e)));
		}
		return species;
	}

	/**
	 * Returns the vehicles relevant to this item
	 * 
	 * @return the vehicles relevant to this item
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Vehicle> getVehicles() {
		if (vehicles==null) {
			vehicles = new ArrayList<>();
			((ArrayList<String>)data.get("vehicles")).forEach(e->vehicles.add(new Vehicle(e)));
		}
		return vehicles;
	}

	/**
	 * Returns the starships relevant to this item
	 * 
	 * @return the starships relevant to this item
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Starship> getStarships() {
		if (starships==null) {
			starships = new ArrayList<>();
			((ArrayList<String>)data.get("starships")).forEach(e->starships.add(new Starship(e)));
		}
		return starships;
	}
	
	/**
	 * Returns the planets relevant to this item
	 * 
	 * @return the planets relevant to this item
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Planet> getPlanets() {
		if (planets==null) {
			planets = new ArrayList<>();
			((ArrayList<String>)data.get("planets")).forEach(e->planets .add(new Planet(e)));
		}
		return planets;
	}
	
	/**
	 * Returns the planets relevant to this item
	 * 
	 * @return the planets relevant to this item
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<People> getPilots() {
		if (pilots==null) {
			pilots  = new ArrayList<>();
			((ArrayList<String>)data.get("pilots")).forEach(e->pilots.add(new People(e)));
		}
		return pilots;
	}
	
	/**
	 * @see Object#equals(Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (o instanceof Item)
			return (((Item)o).getName().equals(getName()));
		return false;
	}
}
