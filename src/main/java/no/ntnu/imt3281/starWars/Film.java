package no.ntnu.imt3281.starWars;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Used to store information about a Star Wars film.
 *  
 * @author oivindk
 *
 */
public class Film extends Item {

	/**
	 * Create a Film object, reading information about the film from the database.
	 * If no such database record exists the information should be read from the WWW.
	 * 
	 * @param id the id of the film to create
	 */
	public Film(String id) {
		super(id);
	}

	/**
	 * Create a Film object with the data from the given json encoded data.
	 * Update/store the information in the database.
	 * 
	 * @param id the film to create.
	 * @param json the data about the film to create.
	 */
	public Film(String id, String json) {
		super(id, json);
	}

	/**
	 * Read a film from the database.
	 */
	public void readFromDB() {
		String sql = "SELECT title, episodeId, openingCrawl, director, producer, releaseDate, characters, planets, "
				+ "starships, vehicles, species FROM film WHERE id=?";
		ResultSet res = DB.select(sql, id);
		try {
			if (res!=null&&res.next()) {
				data.put("title", res.getString("title"));
				data.put("episode_id", res.getInt("episodeId"));
				data.put("opening_crawl", res.getString("openingCrawl"));
				data.put("director", res.getString("director"));
				data.put("producer",  res.getString("producer"));
				data.put("release_date", res.getString("releaseDate"));
				data.put("characters", stringArrayRepresentationToArrayList(res.getString("characters")));
				data.put("planets", stringArrayRepresentationToArrayList(res.getString("planets")));
				data.put("starships", stringArrayRepresentationToArrayList(res.getString("starships")));
				data.put("vehicles", stringArrayRepresentationToArrayList(res.getString("vehicles")));
				data.put("species", stringArrayRepresentationToArrayList(res.getString("species")));
			} else {
				readFromSwapi();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Update or write a film to the database.
	 */
	public void writeToDB() {
		String updateSQL = "UPDATE film SET "
				+ "title=?, episodeId=?, openingCrawl=?, director=?, producer=?, "
				+ "releaseDate=?, characters=?, planets=?, "
				+ "starships=?, vehicles=?, species=? WHERE id=?";
		String insertSQL = "INSERT INTO film (title, episodeId, openingCrawl, director, "
				+ "producer, releaseDate, characters, planets, starships, "
				+ "vehicles, species, id) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		@SuppressWarnings("unchecked")
		Object values[] = {data.get("title"), data.get("episode_id"), data.get("opening_crawl"), data.get("director"), 
				data.get("producer"), data.get("release_date"), ((ArrayList<String>)data.get("characters")).toString(), 
				((ArrayList<String>)data.get("planets")).toString(), ((ArrayList<String>)data.get("starships")).toString(),
				((ArrayList<String>)data.get("vehicles")).toString(), ((ArrayList<String>)data.get("species")).toString(), id };
		
		DB.updateInsert(updateSQL, insertSQL, values);
	}	
}
