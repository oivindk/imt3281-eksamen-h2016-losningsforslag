package no.ntnu.imt3281.starWars;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Used to store vehicles from the Star Wars universe
 * 
 * @author oivindk
 *
 */
public class Vehicle extends Item {

	/**
	 * Create a Vehicle object, reading information about the vehicle from the database.
	 * If no such database record exists the information should be read from the WWW.
	 * 
	 * @param id the id of the vehicle to create
	 */
	public Vehicle(String id) {
		super(id);
	}

	/**
	 * Create a Vehicle object with the data from the given json encoded data.
	 * Update/store the information in the database.
	 * 
	 * @param id the vehicle to create.
	 * @param json the data about the vehicle to create.
	 */
	public Vehicle(String id, String json) {
		super(id, json);
	}

	/**
	 * Read a vehicle from the database.
	 */
	public void readFromDB() {
		String sql = "SELECT name, model, manufacturer, cost, length, maxAtmospheringSpeed, crew, "
				+ "passengers, cargoCapacity, consumables, vehicleClass, pilots, films FROM vehicle WHERE id=?";
		ResultSet res = DB.select(sql, id);
		try {
			if (res!=null&&res.next()) {
				data.put("name", res.getString("name"));
				data.put("model", res.getString("model"));
				data.put("manufacturer", res.getString("manufacturer"));
				data.put("cost_in_credits",  res.getDouble("cost"));
				data.put("length", res.getFloat("length"));
				data.put("max_atmosphering_speed", res.getString("maxAtmospheringSpeed"));
				data.put("crew", res.getInt("crew"));
				data.put("passengers", res.getInt("passengers"));
				data.put("cargo_capacity", res.getInt("cargoCapacity"));
				data.put("consumables", res.getString("consumables"));
				data.put("vehicle_class", res.getString("vehicleClass"));
				data.put("pilots", stringArrayRepresentationToArrayList(res.getString("pilots")));
				data.put("films", stringArrayRepresentationToArrayList(res.getString("films")));
			} else {
				readFromSwapi();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Update or write a vehicle to the database.
	 */
	public void writeToDB() {
		String updateSQL = "UPDATE vehicle SET "
				+ "name=?, model=?, manufacturer=?, cost=?, "
				+ "length=?, maxAtmospheringSpeed=?, crew=?, "
				+ "passengers=?, cargoCapacity=?, consumables=?, vehicleClass=?, "
				+ "pilots=?, films=? WHERE id=?";
		String insertSQL = "INSERT INTO vehicle (name, model, manufacturer, "
				+ "cost, length, maxAtmospheringSpeed, crew, passengers, "
				+ "cargoCapacity, consumables, vehicleClass, pilots, films, id) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		@SuppressWarnings("unchecked")
		Object values[] = {data.get("name"), data.get("model"), data.get("manufacturer"), 
				getCost(), data.get("length"), data.get("max_atmosphering_speed"), data.get("crew"), 
				data.get("passengers"), getCargoCapacity(), data.get("consumables"), data.get("vehicle_class"),  
				((ArrayList<String>)data.get("pilots")).toString(), ((ArrayList<String>)data.get("films")).toString(), id };
		
		DB.updateInsert(updateSQL, insertSQL, values);
	}	
}
