package no.ntnu.imt3281.starWars;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Used to store a starship in the Star Wars universe
 * 
 * @author oivindk
 *
 */
public class Starship extends Item {
	/**
	 * Create a Starship object, reading information about the starship from the database.
	 * If no such database record exists the information should be read from the WWW.
	 * 
	 * @param id the id of the starship to create
	 */
	public Starship (String id) {
		super(id);
	}
	
	
	/**
	 * Create a Starship object with the data from the given json encoded data.
	 * Update/store the information in the database.
	 * 
	 * @param id the starship to create.
	 * @param json the data about the starship to create.
	 */
	public Starship(String id, String json) {
		super(id, json);
	}

	/**
	 * Read a starship from the database.
	 */
	public void readFromDB() {
		String sql = "SELECT name, model, manufacturer, cost, length, maxAtmospheringSpeed, crew, "
				+ "passengers, cargoCapacity, consumables, hyperdriveRating, MGLT, starshipClass, "
				+ "pilots, films FROM starship WHERE id=?";
		ResultSet res = DB.select(sql, id);
		try {
			if (res!=null&&res.next()) {
				data.put("name", res.getString("name"));
				data.put("model", res.getString("model"));
				data.put("manufacturer", res.getString("manufacturer"));
				data.put("cost_in_credits",  res.getDouble("cost"));
				data.put("length", res.getDouble("length"));
				data.put("max_atmosphering_speed", res.getString("maxAtmospheringSpeed"));
				data.put("crew", res.getInt("crew"));
				data.put("passengers", res.getInt("passengers"));
				data.put("cargo_capacity", res.getObject("consumables"));
				data.put("hyperdrive_rating", res.getFloat("hyperdriveRating"));
				data.put("MGLT", res.getInt("MGLT"));
				data.put("starship_class", res.getString("starshipClass"));
				data.put("pilots", stringArrayRepresentationToArrayList(res.getString("pilots")));
				data.put("films", stringArrayRepresentationToArrayList(res.getString("films")));
			} else {
				readFromSwapi();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Update or write a vehicle to the database.
	 */
	public void writeToDB() {
		String updateSQL = "UPDATE starship SET "
				+ "name=?, model=?, manufacturer=?, cost=?, "
				+ "length=?, maxAtmospheringSpeed=?, crew=?, "
				+ "passengers=?, cargoCapacity=?, consumables=?, "
				+ "hyperdriveRating=?, MGLT=?, starshipClass=?, "
				+ "pilots=?, films=? WHERE id=?";
		String insertSQL = "INSERT INTO starship (name, model, manufacturer, "
				+ "cost, length, maxAtmospheringSpeed, crew, passengers, "
				+ "cargoCapacity, consumables, hyperdriveRating, MGLT, starshipClass, pilots, films, id) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		@SuppressWarnings("unchecked")
		Object values[] = {data.get("name"), data.get("model"), data.get("manufacturer"), 
				getCost(), data.get("length"), data.get("max_atmosphering_speed"), data.get("crew"), 
				data.get("passengers"), data.get("cargo_capacity"), data.get("consumables"), 
				data.get("hyperdrive_rating"), data.get("MGLT"), data.get("starship_class"),  
				((ArrayList<String>)data.get("pilots")).toString(), ((ArrayList<String>)data.get("films")).toString(), id };
		
		DB.updateInsert(updateSQL, insertSQL, values);
	}	

	/**
	 * Get the hyperdrive rating for this starship
	 * 
	 * @return the hyperdrive rating
	 */
	public float getHyperdriveRating() {
		return Float.parseFloat(data.get("hyperdrive_rating").toString());
	}

	/**
	 * Get the MGLT for this starship
	 * 
	 * @return the MGLT
	 */
	public int getMGLT() {
		return Integer.parseInt(data.get("MGLT").toString());
	}

	/**
	 * Get the starship class
	 * 
	 * @return the starship class
	 */
	public String getStarshipClass() {
		return data.get("starship_class").toString();
	}

}
