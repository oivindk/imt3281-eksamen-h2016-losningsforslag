package no.ntnu.imt3281.starWars;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Stores information about a species in the star wars universe
 * 
 * @author oivindk
 *
 */
public class Species extends Item {
	private ArrayList<People> people = null;

	/**
	 * Create a Species object, reading information about the species from the database.
	 * If no such database record exists the information should be read from the WWW.
	 * 
	 * @param id the id of the species to create
	 */
	public Species(String id) {
		super(id);
	}

	/**
	 * Create a Species object with the data from the given json encoded data.
	 * Update/store the information in the database.
	 * 
	 * @param id the species to create.
	 * @param json the data about the film to create.
	 */
	public Species(String id, String json) {
		super(id, json);
	}

	/**
	 * Read a species from the database.
	 */
	public void readFromDB() {
		String sql = "SELECT name, classification, designation, averageHeight, skinColors, hairColors, eyeColors, "
				+ "averageLifespan, homeworld, language, people, films FROM species WHERE id=?";
		ResultSet res = DB.select(sql, id);
		try {
			if (res!=null&&res.next()) {
				data.put("name", res.getString("name"));
				data.put("classification", res.getString("classification"));
				data.put("designation", res.getString("designation"));
				data.put("average_height",  res.getInt("averageHeight"));
				data.put("skin_colors", res.getString("skinColors"));
				data.put("hair_colors", res.getString("hairColors"));
				data.put("eye_colors", res.getString("eyeColors"));
				data.put("average_lifespan", res.getString("averageLifespan"));
				data.put("homeworld", res.getString("homeworld"));
				data.put("language", res.getString("language"));
				data.put("people", stringArrayRepresentationToArrayList(res.getString("people")));
				data.put("films", stringArrayRepresentationToArrayList(res.getString("films")));
			} else {
				readFromSwapi();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Update or write a species to the database.
	 */
	public void writeToDB() {
		String updateSQL = "UPDATE species SET "
				+ "name=?, classification=?, designation=?, averageHeight=?, "
				+ "skinColors=?, hairColors=?, eyeColors=?, "
				+ "averageLifespan=?, homeworld=?, language=?, people=?, "
				+ "films=? WHERE id=?";
		String insertSQL = "INSERT INTO species (name, classification, designation, "
				+ "averageHeight, skinColors, hairColors, eyeColors, averageLifespan, "
				+ "homeworld, language, people, films, id) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		@SuppressWarnings("unchecked")
		Object values[] = {data.get("name"), data.get("classification"), data.get("designation"), 
				getaverageHeight(), data.get("skin_colors"), data.get("hair_colors"), data.get("eye_colors"), 
				getAverageLifespan(), data.get("homeworld"), data.get("language"), 
				((ArrayList<String>)data.get("people")).toString(), ((ArrayList<String>)data.get("films")).toString(), id };
		
		DB.updateInsert(updateSQL, insertSQL, values);
	}	

	/**
	 * Returns the classification of this species
	 * 
	 * @return the classification of this species
	 */
	public String getClassification() {
		return data.get("classification").toString();
	}

	/**
	 * Returns the designation of this species
	 * 
	 * @return the designation of this species
	 */
	public Object getDesignation() {
		return data.get("designation").toString();
	}
	
	/**
	 * Returns the skin colors of this species
	 * 
	 * @return the skin colors of this species
	 */
	public String getSkinColors() {
		return data.get("skin_colors").toString();
	}

	/**
	 * Returns the hair colors of this species
	 * 
	 * @return the hair colors of this species
	 */
	public String getHairColors() {
		return data.get("hair_colors").toString();
	}

	/**
	 * Returns the eye colors of this species
	 * 
	 * @return the eye colors of this species
	 */
	public String getEyeColors() {
		return data.get("eye_colors").toString();
	}

	/**
	 * Returns the language of this species
	 * 
	 * @return the language of this species
	 */
	public String getLanguage() {
		return data.get("language").toString();
	}

	/**
	 * Returns the average height of this species
	 * 
	 * @return the average height of this species
	 */
	public int getaverageHeight() {
		if (data.get("average_height").toString().equals("n/a"))
			return -1;
		return Integer.parseInt(data.get("average_height").toString());
	}

	/**
	 * Returns the average lifespan of this species
	 * 
	 * @return the average lifespan of this species
	 */
	public String getAverageLifespan() {
		return data.get("average_lifespan").toString();
	}

	
	/**
	 * Returns a list of people of this species
	 * 
	 * @return an ArrayList of People of this species
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<People> getPeople() {
		if (people==null) {
			people = new ArrayList<>();
			((ArrayList<String>)data.get("people")).forEach(e->people .add(new People(e)));
		}
		return people;	
	}
}
