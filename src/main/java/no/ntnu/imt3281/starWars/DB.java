package no.ntnu.imt3281.starWars;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * All Database operations in the program goes through this class.
 * 
 * @author oivindk
 *
 */
public class DB {
	private static final String URL = "jdbc:derby:StarWarsDB";
	private static Connection con;
	@SuppressWarnings("unused")
	private static DB db = new DB();
	
	private DB() {
		try {
			con = DriverManager.getConnection(URL);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Used to update or insert a new record into the database.
	 * Takes two SQL statements, an update statement and an insert statement, 
	 * if the update statement updates a record no record is inserted but if
	 * no update happens that means we should insert a new record.
	 * 
	 * @param updateSQL the statement used for updating the database
	 * @param insertSQL the statement to run if no record was updated
	 * @param values the values to update with (or insert) the database with
	 */
	public static void updateInsert(String updateSQL, String insertSQL, Object[] values) {
		try {
			PreparedStatement stmt = con.prepareStatement(updateSQL);
			for (int i=0; i<values.length; i++)
				stmt.setObject(i+1, values[i]);
			int rows = stmt.executeUpdate();
			if (rows==0) {
				stmt = con.prepareStatement(insertSQL);
				for (int i=0; i<values.length; i++)
					stmt.setObject(i+1, values[i]);
				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(insertSQL);
			for (Object o : values) 
				System.out.println(o);
		}		
	}

	/**
	 * Gets data from the database.
	 * 
	 * @param sql the sql used to fetch data from the database.
	 * @param id the id of the record to getch from the database.
	 * @return	the resultset with data from the database.
	 */
	public static ResultSet select(String sql, String id) {
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, id);
			ResultSet res = stmt.executeQuery();
			return res;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
