package no.ntnu.imt3281.starWars;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Used to store information about a planet in the star wars universe.
 * 
 * @author oivindk
 *
 */
public class Planet extends Item {
	private ArrayList<People> residents = null;

	/**
	 * Reads a planet from the database. If no planet with given id exists in the database
	 * it should be read from WWW.
	 * 
	 * @param id the id of the planet to initialize
	 */
	public Planet(String id) {
		super(id);
	}

	/**
	 * Create a planet from the given json data. Update/store the planet information
	 * in the database.
	 * 
	 * @param id the id of the planet to create and store 
	 * @param json the data about the planet to create and store
	 */
	public Planet(String id, String json) {
		super (id, json);
	}

	/**
	 * Read a planet from the database.
	 */
	public void readFromDB() {
		String sql = "SELECT name, rotationPeriod, orbitalPeriod, diameter, climate, gravity, terrain, surfaceWater, "
				+ "population, residents, films FROM planet WHERE id=?";
		ResultSet res = DB.select(sql, id);
		try {
			if (res!=null&&res.next()) {
				data.put("name", res.getString("name"));
				data.put("rotation_period", res.getInt("rotationPeriod"));
				data.put("orbital_period", res.getInt("orbitalPeriod"));
				data.put("diameter", res.getInt("diameter"));
				data.put("climate", res.getString("climate"));
				data.put("gravity",  res.getString("gravity"));
				data.put("terrain", res.getString("terrain"));
				data.put("surface_water", res.getInt("surfaceWater"));
				data.put("population", res.getLong("population"));
				data.put("residents", stringArrayRepresentationToArrayList(res.getString("residents")));
				data.put("films", stringArrayRepresentationToArrayList(res.getString("films")));
			} else {
				readFromSwapi();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Update or write a film to the database.
	 */
	public void writeToDB() {
		String updateSQL = "UPDATE planet SET "
				+ "name=?, rotationPeriod=?, orbitalPeriod=?, diameter=?, climate=?, "
				+ "gravity=?, terrain=?, surfaceWater=?, "
				+ "population=?, residents=?, films=? WHERE id=?";
		String insertSQL = "INSERT INTO planet (name, rotationPeriod, orbitalPeriod, diameter, "
				+ "climate, gravity, terrain, surfaceWater, population, "
				+ "residents, films, id) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		@SuppressWarnings("unchecked")
		Object values[] = {data.get("name"), data.get("rotation_period"), data.get("orbital_period"), data.get("diameter"), 
				data.get("climate"), data.get("gravity"), data.get("terrain"), data.get("surface_water"), 
				getPopulation(), ((ArrayList<String>)data.get("residents")).toString(), 
				((ArrayList<String>)data.get("films")).toString(), id };
		
		DB.updateInsert(updateSQL, insertSQL, values);
	}	
			
	/**
	 * Returns the rotation period for this planet
	 * 
	 * @return the rotation period for this planet
	 */
	public int getRotationPeriod() {
		return Integer.parseInt(data.get("rotation_period").toString());
	}

	/**
	 * Returns the rotation period for this planet
	 * 
	 * @return the rotation period for this planet
	 */
	public int getOrbitalPeriod() {
		return Integer.parseInt(data.get("orbital_period").toString());
	}

	/**
	 * Returns the diameter of this planet
	 * 
	 * @return the diameter of this planet
	 */
	public int getDiameter() {
		return Integer.parseInt(data.get("diameter").toString());
	}

	/**
	 * Returns the climate of this planet
	 * 
	 * @return the climate of this planet
	 */
	public String getClimate() {
		return data.get("climate").toString();
	}

	/**
	 * Returns the gravity of this planet
	 * 
	 * @return the gravity of this planet
	 */
	public String getGravity() {
		return data.get("gravity").toString();
	}

	/**
	 * Returns the terrain of this planet
	 * 
	 * @return the terrain of this planet
	 */
	public String getTerrain() {
		return data.get("terrain").toString();
	}

	/**
	 * Returns the surface water of this planet
	 * 
	 * @return the surface water of this planet
	 */
	public int getSurfaceWater() {
		return Integer.parseInt(data.get("surface_water").toString());
	}
	
	/**
	 * Returns the population of this planet
	 * 
	 * @return the population of this planet
	 */
	public long getPopulation() {
		if (data.get("population").toString().equals("unknown"))
			return -1;
		return Long.parseLong(data.get("population").toString());
	}

	/**
	 * Returns the residents of this planet
	 * 
	 * @return an ArrayList of People inhabitationg this planet.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<People> getResidents() {
		if (residents==null) {
			residents = new ArrayList<>();
			((ArrayList<String>)data.get("residents")).forEach(e->residents.add(new People(e)));
		}
		return residents;	
	}
}
