package no.ntnu.imt3281.starWars;

import java.io.IOException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class StarWars {
	ObservableList<Film> filmData = FXCollections.observableArrayList ();
	ObservableList<People> peopleData = FXCollections.observableArrayList();
	ObservableList<Planet> planetData = FXCollections.observableArrayList();
	ObservableList<Starship> starshipData = FXCollections.observableArrayList();
	ObservableList<Vehicle> vehicleData = FXCollections.observableArrayList();
	ObservableList<Species> speciesData = FXCollections.observableArrayList();
	
	// Tabs
	@FXML private TabPane tabPane;
    @FXML private Tab moviesTab;
    @FXML private Tab charactersTab;
    @FXML private Tab planetsTab;
    @FXML private Tab starhipsTab;
    @FXML private Tab vehiclesTab;
    @FXML private Tab speciesTab;

    // Movies tab
    @FXML private Label mReleaseDate;
    @FXML private Label mDirector;
    @FXML private Label mOpeningCrawl;
    @FXML private Label mProducer;
    @FXML private ComboBox<Film> mFilms;
    @FXML private ComboBox<People> mCharacters;
    @FXML private ComboBox<Planet> mPlanets;
    @FXML private ComboBox<Starship> mStarships;
    @FXML private ComboBox<Species> mSpecies;
    @FXML private ComboBox<Vehicle> mVehicles;
    @FXML private ImageView mImage;
 
    // Character tab
    @FXML private Label cName;
    @FXML private Label cHeight;
    @FXML private Label cMass;
    @FXML private Label cHairColor;
    @FXML private Label cSkinColor;
    @FXML private Label cEyeColor;
    @FXML private Label cBirthYear;
    @FXML private Label cGender;
    @FXML private Button cHomeworld;
    @FXML private ComboBox<Film> cFilms;
    @FXML private ComboBox<Species> cSpecies;
    @FXML private ComboBox<Vehicle> cVehicles;
    @FXML private ComboBox<Starship> cStarships;
    @FXML private ImageView cImage;
    
    @FXML	// Oppgave 4
    public void initialize() {
    	for (int i=1; i<=7; i++) 
    		filmData.add(new Film("films/"+i+"/"));
    	mFilms.setItems(filmData);
    }
    
    @FXML	// Oppgave 5
    public void filmSelected(ActionEvent ae) {
    	ComboBox<Film> source = (ComboBox<Film>)ae.getSource();
    	Film f = source.getSelectionModel().getSelectedItem();
    	if (f!=null) {
	    	mReleaseDate.setText(f.getReleaseDate());
	    	mDirector.setText(f.getDirector());
	    	mProducer.setText(f.getProducer());
	    	peopleData.clear();
	    	for (People p : f.getCharacters())
	    		peopleData.add(p);
	    	mCharacters.setItems(peopleData);
	    	planetData.clear();
	    	for (Planet p : f.getPlanets())
	    		planetData.add(p);
	    	mPlanets.setItems(planetData);
	    	starshipData.clear();
	    	for (Starship s : f.getStarships())
	    		starshipData.add(s);
	    	mStarships.setItems(starshipData);
	    	vehicleData.clear();
	    	for (Vehicle v : f.getVehicles())
	    		vehicleData.add(v);
	    	mVehicles.setItems(vehicleData);
	    	speciesData.clear();
	    	for (Species s : f.getSpecies())
	    		speciesData.add(s);
	    	mSpecies.setItems(speciesData);
	    	mOpeningCrawl.setText(f.getOpeningCrawl());
	    	tabPane.getSelectionModel().select(moviesTab);
    	}
    }
    
    @FXML	// Oppgave 6
    void characterSelected(ActionEvent ae) {
    	ComboBox<People> source = (ComboBox<People>)ae.getSource();
    	People p = source.getSelectionModel().getSelectedItem();
    	if (p!=null) {
	    	cName.setText(p.getName());
	    	cHeight.setText(Integer.toString(p.getHeight()));
	    	cMass.setText(Float.toString(p.getMass()));
	    	cHairColor.setText(p.getHairColor());
	    	cSkinColor.setText(p.getSkinColor());
	    	cEyeColor.setText(p.getEyeColor());
	    	cBirthYear.setText(p.getBirthYear());
	    	cGender.setText(p.getGender());
	    	cHomeworld.setText(p.getHomeworld().getName());
	    	filmData.clear();
	    	for (Film f : p.getFilms())
	    		filmData.add(f);
	    	cFilms.setItems(filmData);
	    	    	starshipData.clear();
	    	for (Starship s : p.getStarships())
	    		starshipData.add(s);
	    	cStarships.setItems(starshipData);
	    	vehicleData.clear();
	    	for (Vehicle v : p.getVehicles())
	    		vehicleData.add(v);
	    	cVehicles.setItems(vehicleData);
	    	speciesData.clear();
	    	for (Species s : p.getSpecies())
	    		speciesData.add(s);
	    	cSpecies.setItems(speciesData);
	    	Document doc;
			try {
				doc = Jsoup.connect("http://starwars.wikia.com/wiki/"+p.getName().replaceAll(" ", "_")).get();
				Element e = doc.select("#WikiaMainContent aside figure a img").first();
				if (e!=null&&e.attr("src")!=null) {
					URL u = new URL(e.attr("src"));
					cImage.setImage(new Image(u.openStream()));
					Platform.runLater(()->
						cImage.setLayoutX((int)((595-cImage.getImage().getWidth())/2))
					);
				} else
					cImage.setImage(null);
			} catch (IOException e1) {
				cImage.setImage(null);
			}
	    	tabPane.getSelectionModel().select(charactersTab);
    	}
    }
}